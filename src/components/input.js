import React from 'react';
import { InputGroup, FormControl } from 'react-bootstrap';

const Input = ({placeholder}) => {
    return (
        <div>
            <InputGroup  className="inputBox">
                <FormControl
                    placeholder={placeholder}
                />
            </InputGroup>
        </div>
    );
};

export default Input;