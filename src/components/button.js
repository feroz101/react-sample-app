import React from 'react';
import {Button} from 'react-bootstrap';

const AppButton = ({name,type}) => {
    return (
            <Button className="appBtn" variant={type}>{name}</Button>
    );
};

export default AppButton;