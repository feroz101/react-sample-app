import React from 'react';

const SocialLogo = ({type}) => {
    return (
         <div className="social-logo">
              <i className={type}></i>       
        </div>
    );
};

export default SocialLogo;