import React from 'react';
import SocialLogo from './sociallogo';

const SocialLogin = () => {
    return (
        <div>
          <SocialLogo type="fab fa-google-plus"/>
          <SocialLogo type="fab fa-twitter"/>
          <SocialLogo type="fab fa-instagram"/>
          <SocialLogo type="fab fa-facebook"/>
          
      
        </div>
    );
};

export default SocialLogin;