import React  from 'react';
import {Image} from 'react-bootstrap';

const Logo=({url}) =>{
    
    return(
    <div>
     <Image className="logo" src={url}/> 
    </div>
    );
};


export default Logo;