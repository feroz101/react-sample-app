import React, { Component } from 'react';
import { Navbar, Nav, Form, FormControl, Button, InputGroup, Col, Row } from 'react-bootstrap';
import './styles.css';


class AppNavbar extends Component {
constructor(props){
    
    super(props);
    this.state={
        toggler:false
        }

    this.handle=this.handle.bind(this);
}


handle=()=>{
      console.log('handled')
    this.setState({
        toggler:true
    })

}

render(){
    return(
        
        <Navbar className = "navbar" expand = "lg" fixed = "top" >
                    <Row>
                        <Col>
                            <Navbar.Brand href="#home">MOBICART</Navbar.Brand>
                        </Col>
        
                        <Col style={{ padding: 10, fontSize: 20 }}>
        
                        </Col>
                    </Row>
        
        
                    <span onClick={this.handle} className={this.state.toggle} data-toggle="collapse" data-target="#collapsingNav"><i className=" fas fa-bars"></i></span>
        
        
                    <Navbar.Collapse id="collapsingNav">
                        <Col>
                            <Nav className="nav mr-auto">
                                <Nav.Link href="/">HOME</Nav.Link>
                                <Nav.Link href="#link">OFFERS</Nav.Link>
                                <Nav.Link href="#link">DEALS</Nav.Link>
        
                            </Nav>
                        </Col>
        
                        <Col sm={8}>
                            <Nav className="nav mr-auto">
                                <Form inline className="col-sm-12 col-xs-12 col-md-12">
                                    <InputGroup className="col-sm-12 col-xs-12">
                                        <FormControl className="search-input"
                                            placeholder="search smartphones,accessories and more..."
                                            aria-label="Recipient's username"
                                        />
                                        <InputGroup.Append>
                                            <Button className="search-btn" variant="outline-secondary"><i className="fas fa-search"></i></Button>
                                        </InputGroup.Append>
                                    </InputGroup>
        
                                </Form>
                            </Nav>
                        </Col>
                        <Col>
                            <Nav className="nav float-sm-right">
                                <Nav.Link href="#home"><i className="fas fa-cart-plus"></i></Nav.Link>
                                <Nav.Link href="/sign_in">Sign In</Nav.Link>
                            </Nav>
                        </Col>
                    </Navbar.Collapse>
                </Navbar>
            );
        };
        
}

export default AppNavbar;