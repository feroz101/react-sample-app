import React from 'react';
import {Card} from 'react-bootstrap';
import AppButton from './button';
import './styles.css';

const MobileCard = ({img,name,location}) => {
    return (
        <div>
       <Card className="mobile-card" style={{ width: '18rem' }}>
                                <Card.Img variant="top" src={img} />
                                <Card.Body>
                                    <Card.Title>{name}</Card.Title>
                                    <Card.Text>
                                        {location} 
                                      </Card.Text>
                                      <Card.Text>
                                       <span>$200.00</span>
                                    
                                      </Card.Text>
                                     
                                    <AppButton name="Shop Now" type="danger">Shop Now</AppButton>
                                    <AppButton name="Add To Cart" type="primary">Shop Now</AppButton>

                                </Card.Body>
                            </Card>
      
        </div>
    );
};

export default MobileCard;