import {
    BrowserRouter as Router,
    Route,
    Switch
}
    from "react-router-dom";
import React
    from 'react';
import {
    Login,
    Register,
    Welcome
}
    from '../screens';
import {
    AppNavbar
}
    from '../components';


const Routes=()=> {
    return (
            <div style={{backgroundColor:'#E9ECEF'}}>
                <AppNavbar />
                <Router>
                    <Switch>
                        <Route path="/" component={Welcome} exact></Route>
                        <Route path="/sign_in" component={Login} exact></Route>
                        <Route path="/sign_up" component={Register} exact></Route>
                    </Switch>
                </Router>
              
            </div>
        );
}


export default Routes;
