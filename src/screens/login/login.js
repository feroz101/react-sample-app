import React from 'react';
import { Col} from 'react-bootstrap';
import {Logo,AppButton,Input,SocialLogin} from '../../components';
import logo from '../../assets/logo.svg';
import './styles.css';

class Login extends React.Component {
    render() {
        return (
            <div className="login-container">
            <Col xs={12} md={3}>
            <form className="form-signin">
                <Logo className="logo" url={logo} />
                <span>Sign In</span>
                <Input placeholder="enter email"/>
                <Input placeholder="enter password"/>
                <div>
                <AppButton name="LOGIN" type="primary"/> 
                <a href="/">Create New Account</a>
                 <br/>
                <span>Or Sign In with</span>
                <SocialLogin/>
                </div>
         </form>
            </Col>
</div>
        );
    }
}


export default Login;
