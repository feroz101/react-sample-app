import React from 'react';
import { Col, Jumbotron, Image,Row} from 'react-bootstrap';
import { AppButton,MobileCard } from '../../components';

import './styles.css';

class Welcome extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: []
        };
    }

    componentDidMount() {

        fetch('https://randomuser.me/api/?results=20')
          .then(results => {
            return results.json()
          })
          .then(data => {
            let users = data.results.map((user) => {
              return (
              
                <Col className="col" sm={3} xs={12} key={user.email}>
                  <MobileCard img={user.picture.medium} name={user.name.first + ' ' + user.name.last} city={user.location.city + ' ' + user.location.state} />
                </Col>
              )
            })
            this.setState({
              users: users
            })
            console.log('state', this.state.pictures)
    
          }
          ).catch(err => {
            console.log(err)
          })
    
        this.setState({
          isShow: !this.state.isShow
        })
        setInterval(() => {
          this.setState({
            isShow: false
          })
        }, 2000);
      }
    


    render() {
        return (
            <div className="main">

                <Jumbotron>
                    <Row>
                        <Col className="col" sm={6} xs={6}>
                            <Image className="intro-img" src="http://pngimg.com/uploads/smartphone/smartphone_PNG8513.png" />
                        </Col>
                        <Col className="col intro" sm={6} xs={6}>
                            <p>
                                <span>MOBICART</span><br />
                                Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.</p>
                            <AppButton name="Shop Now" type="danger" />
                        </Col>
                    </Row>
                </Jumbotron>


                <Jumbotron>
                    <Row>
                        <Col>Featured Products</Col>
                    </Row>
                    <Row>
                          
                         {this.state.users}

                    </Row>
                </Jumbotron>


                <Jumbotron>
                    <Row>
                        <Col className="col" sm={6} xs={12}>
                            <Image className="intro-img" src="http://pngimg.com/uploads/smartphone/smartphone_PNG8513.png" />
                        </Col>
                        <Col className="col intro" sm={6} xs={12}>
                            <p>
                                <span>MOBICART</span><br />
                                Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups.</p>
                            <AppButton name="Shop Now" type="danger" />
                        </Col>
                    </Row>
                </Jumbotron>




            </div>

        );
    }
}


export default Welcome;
